import java.util.Stack;

import static java.lang.String.format;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   public static Node parsePostfix (String s) {
      validateTree(s);
      Stack<Node> stack = new Stack<>();
      Node currentNode = new Node(null, null, null);
      boolean rootClosed = false;

      for (Character token : s.toCharArray()) {
         switch (token) {
            case '(':
               if (rootClosed) {
                  throw new RuntimeException("Root element was already closed");
               }
               stack.push(currentNode);
               currentNode.firstChild = new Node(null, null, null);
               currentNode = currentNode.firstChild;
               break;
            case ')':
               currentNode = stack.pop();
               if (stack.size() == 0) {
                  rootClosed = true;
               }
               break;
            case ',':
               if (rootClosed) {
                  throw new RuntimeException("Root element was already closed");
               }
               currentNode.nextSibling = new Node(null, null, null);
               currentNode = currentNode.nextSibling;
               break;
            default:
               currentNode.name = currentNode.name == null ? "" + token : currentNode.name + token;
               break;
         }
      }
      return currentNode;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }

   public String toXml(int root) {
      String open = "<L" + root + ">";
      String close = "<L" + root + "/>";

      StringBuilder result = new StringBuilder();
      result.append("\t".repeat(root - 1)).append(open);
      result.append(" ").append(this.name);

      if (this.firstChild != null) {
         result.append("\n");
         result.append(this.firstChild.toXml(root + 1));
      }
      if (this.firstChild != null && this.nextSibling != null) {
         result.append("\t".repeat(root - 1)).append(close).append("\n");
         result.append(this.nextSibling.toXml(root));
      } else if (this.nextSibling != null) {
         result.append(" ").append(close).append("\n");
         result.append(this.nextSibling.toXml(root));
      } else if (this.firstChild == null) {
         result.append(" ").append(close).append("\n");
      } else {
         result.append("\t".repeat(root - 1)).append(close).append("\n");
      }
      if (root == 1) {
         result.deleteCharAt(result.length() - 1);
      }
      return result.toString();
   }

   private static void validateTree(String tree) {
      if (tree.isEmpty()) {
         throw new RuntimeException("Empty expression");
      } else if (tree.contains(",,")) {
         throw new RuntimeException(format("Double commas in expression %s", tree));
      } else if (tree.contains(",") && !(tree.contains("(") && tree.contains(")"))) {
         throw new RuntimeException("No brackets in expression %s");
      } else if (tree.contains("\t")) {
         throw new RuntimeException(format("Tab character in expression %s", tree));
      } else if (tree.contains("()")) {
         throw new RuntimeException(format("Empty subtree in expression %s", tree));
      } else if (tree.contains(" ")) {
         throw new RuntimeException(format("Space character in expression %s", tree));
      } else if (tree.contains("))")) {
         throw new RuntimeException(format("Double bracket in expression %s", tree));
      } else if (tree.contains(",)")) {
         throw new RuntimeException(format("No siblings in expression %s", tree));
      } else if (tree.contains("(,")) {
         throw new RuntimeException(format("No siblings in expression %s", tree));
      }
   }
}
